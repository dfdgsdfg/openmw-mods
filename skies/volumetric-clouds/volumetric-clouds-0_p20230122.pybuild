# Copyright 2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.git import Git
from common.mw import MW, InstallDir


class Package(Git, MW):
    NAME = "Zesterer's Volumetric Cloud & Mist"
    DESC = "Adds raycasted volumetric clouds to Morrowind"
    HOMEPAGE = "https://github.com/zesterer/openmw-volumetric-clouds"
    GIT_SRC_URI = "https://github.com/zesterer/openmw-volumetric-clouds.git"
    GIT_COMMIT_DATE = "2023-01-22"
    LICENSE = "all-rights-reserved"
    RDEPEND = "base/morrowind"
    INSTALL_DIRS = [InstallDir(".", S="openmw-volumetric-clouds", DOC=["README.md"])]

    def pkg_pretend(self):
        super().pkg_pretend()

        self.warn(
            "This mod only works with OpenMW 0.48 (currently a development build)"
        )
