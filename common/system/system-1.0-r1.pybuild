# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import shutil
import sys

from pybuild import Pybuild2, use_reduce, version_gt


class Package(Pybuild2):
    NAME = "System"
    DESC = "Pybuild Class for system-installed executables"
    KEYWORDS = "openmw tes3mp"


class System(Pybuild2):
    """
    Pybuild Class for system-installed executables
    """

    RESTRICT = "fetch"
    RELEASE_PAGE: str

    def pkg_pretend(self):
        super().pkg_pretend()
        exec_path = shutil.which(self.PN)

        if not exec_path:
            string = os.linesep.join(
                [
                    f"Unable to find executable {self.PN}",
                    "Please download and install it using your package manager",
                    "or from the following location(s):",
                ]
            )
            for source in use_reduce(self.RELEASE_PAGE, self.USE):
                string += f"    {source}" + os.linesep

            self.warn(string)

    def get_version(self) -> str:
        """Returns the version string for the software"""
        return self.PV

    def src_prepare(self):
        super().src_prepare()
        exec_path = shutil.which(self.PN)

        if not exec_path:
            print(f"ERROR: Could not find executable {self.PN}")
            sys.exit(1)

        version = self.get_version()
        if version_gt(self.PV, version):
            print(
                f"ERROR: {self.PN} has version {version}, but we require >= {self.PV}"
            )
            sys.exit(1)
