# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Blighted Animals Retextured"
    DESC = "Adds unique textures to all blighted animals"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/42245"
    LICENSE = "all-rights-reserved"
    RDEPEND = "base/morrowind"
    KEYWORDS = "openmw"
    SRC_URI = """
        Nix-Hound_knockout_animation_fix-42245-.7z
        texture_size_256? ( Blighted_Animals_with_Vanilla_textures-42245-1-2.7z )
        texture_size_512? ( Blighted_Animals_with_Darknuts_512_textures-42245-1-2.7z )
        texture_size_1024? (
            Blighted_Animals_with_Darknuts_1024_textures-42245-1-2.7z
        )
    """
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/42245"
    TEXTURE_SIZES = "256 512 1024"
    INSTALL_DIRS = [
        InstallDir(".", S="Nix-Hound_knockout_animation_fix-42245-"),
        InstallDir(
            ".",
            PLUGINS=[File("BlightedAnimalsRetextured.esp")],
            S="Blighted_Animals_with_Vanilla_textures-42245-1-2",
            REQUIRED_USE="texture_size_256",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("BlightedAnimalsRetextured.esp")],
            S="Blighted_Animals_with_Darknuts_512_textures-42245-1-2",
            REQUIRED_USE="texture_size_512",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("BlightedAnimalsRetextured.esp")],
            S="Blighted_Animals_with_Darknuts_1024_textures-42245-1-2",
            REQUIRED_USE="texture_size_1024",
        ),
    ]

    def src_prepare(self):
        # This file conflicts with the main ReadMe.txt
        os.chdir(os.path.join(self.WORKDIR, "Nix-Hound_knockout_animation_fix-42245-"))
        os.rename("readme.txt", "readme-nix-hound-knockout-fix.txt")
